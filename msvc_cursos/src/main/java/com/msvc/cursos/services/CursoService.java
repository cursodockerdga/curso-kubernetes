package com.msvc.cursos.services;

import com.msvc.cursos.models.Usuario;
import com.msvc.cursos.models.entity.Curso;

import java.util.List;
import java.util.Optional;

public interface CursoService {

    List<Curso> Listar();
    Optional<Curso> porId(Long id);
    Curso guardar (Curso curso);
    void Eliminar (Long id);

    //Agregando metodos curso-usuario
    Optional<Usuario> asignarUsuario(Usuario usuario, Long cursoId);
    Optional<Usuario> crearUsuario(Usuario usuario,Long cursoId);
    Optional<Usuario> eliminarUsuario(Usuario usuario, Long cursoId); //elimina el usuario del curso add

    Optional<Curso> porIdConUsuarios(Long id);
    void eliminarCursoUsuarioPorId(Long id);
}
