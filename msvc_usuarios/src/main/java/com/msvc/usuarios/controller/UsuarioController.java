package com.msvc.usuarios.controller;


import com.msvc.usuarios.models.entity.Usuario;
import com.msvc.usuarios.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
public class UsuarioController {

    @Autowired
    private UsuarioService service;


    @GetMapping
    public Map<String , List<Usuario>> listar(){
    System.out.println("Estamos probando cambio con Dockerfile Compose!!");
        return Collections.singletonMap("Usuarios",service.listar());
    }

   /* @GetMapping
    public List<Usuario> listar(){

        return service.listar();
    }*/

    @GetMapping("/{id}")
    public ResponseEntity<?> detalle(@PathVariable(name="id") Long id){
        Optional<Usuario> usuarioOptional = service.porId(id);
        return usuarioOptional.isPresent() ? ResponseEntity.ok().body(usuarioOptional.get()) : ResponseEntity.notFound().build();
    }

    @PostMapping
    //@ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> crear(@Valid @RequestBody Usuario usuario, BindingResult result){


        if(result.hasErrors()){
            return Validar(result);
        }

        if(!usuario.getEmail().isEmpty() &&
                //service.porEmail(usuario.getEmail()).isPresent()
                service.existsByEmail(usuario.getEmail())
        )
        {
            return ResponseEntity.badRequest().body(Collections.singletonMap("Mensaje","Ya existe usuario con ese email."));
        }


        return ResponseEntity.status(HttpStatus.CREATED).body(service.guardar(usuario));
    }

    @PutMapping("/{id}")
    //@ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> editar(@Valid @RequestBody Usuario usuario,BindingResult result,@PathVariable Long id){



        if(result.hasErrors()){
            return Validar(result);
        }

        Optional<Usuario> o = service.porId(id);
        if(o.isPresent()){
            Usuario usuarioDb = o.get();

            if(!usuario.getEmail().isEmpty() &&
                    !usuario.getEmail().equalsIgnoreCase(usuarioDb.getEmail()) &&
                    service.porEmail(usuario.getEmail()).isPresent()){
                return ResponseEntity.badRequest()
                        .body(Collections.singletonMap("Mensaje","Ya existe!! usuario con ese email."));
            }


            usuarioDb.setNombre(usuario.getNombre());
            usuarioDb.setEmail(usuario.getEmail());
            usuarioDb.setPassword(usuario.getPassword());
            return ResponseEntity.status(HttpStatus.CREATED).body(service.guardar(usuarioDb));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> eliminar(@PathVariable Long id){
        Optional<Usuario> o = service.porId(id);
        if(o.isPresent()){
            service.eliminar(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();

    }

    @GetMapping("/usuarios-por-curso")
    public ResponseEntity<?> ObtenerUsuariosporCurso(@RequestParam List<Long> ids){
        return ResponseEntity.ok(service.listarporIds(ids));
    }


    private ResponseEntity<Map<String,String>> Validar(BindingResult result) {
        Map<String, String> errores = new HashMap<>();
        result.getFieldErrors().forEach(error -> {
            errores.put(error.getField(), "El campo " + error.getField() + " " + error.getDefaultMessage());
        });
        return ResponseEntity.badRequest().body(errores);
    }

}