package com.msvc.usuarios.services;

import com.msvc.usuarios.models.entity.Usuario;

import java.util.List;
import java.util.Optional;

public interface UsuarioService {
    List<Usuario> listar();
    Optional<Usuario> porId(Long id);
    Usuario guardar (Usuario usuario);
    void eliminar(Long id);

    //Validaciones
    Optional<Usuario> porEmail(String email);
    boolean existsByEmail(String email);

    //Curso-Usuario
    List<Usuario> listarporIds(Iterable<Long> ids);
}
